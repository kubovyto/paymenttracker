Payment tracker
---------------------------------------------------------

This program is used for tracking and storing payments. The payment amounts are stored in different currencies.


Program sources
---------------------------------------------------------
requires Java 1.7
This program uses only external public libraries accessible by maven public repository.




How to run:
---------------------------------------------------------
compilation: in folder where the pom.xml file is stored type
                   -> mvn package

run:  to run program, type
                   -> java -jar target\payment-tracker.jar (optionally -file target\payments.txt to import payments from file)

if error occure, the error message is printed to the console output but the program continue


Usage:
---------------------------------------------------------
- after each command press enter key

- To insert payment, type currency format and the amount in format like CURRENCY AMOUNT, where currency is 3-letter uppercase word and numeric amount.
    Example:  USD 200

- Every currency can contain an exchange rate to currency sets in TrackerCli.TrackerConfig.
  To set exchange rate, type eq. "CZE 0.0035". The CZE currency is converted to selected currency while printing to console.

Other options:

usage: Payment tracker
 -f,--file <arg>   Import payments from file. eg: file payments.txt
 -h,--help         Print commands help.
 -l,--list         Print list of payments.
 -quit             Terminates the program
 -r,--rate <arg>   Set exchange rate to USD. eg: CZE 0.035