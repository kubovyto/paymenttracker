package cz.bsc.paymenttracker;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;

import cz.bsc.paymenttracker.command.Command;
import cz.bsc.paymenttracker.command.CommandParser;
import cz.bsc.paymenttracker.command.CommandProcessor;
import cz.bsc.paymenttracker.command.CommandType;
import cz.bsc.paymenttracker.exception.InvalidCommandException;
import cz.bsc.paymenttracker.factory.BeanFactory;


/**
 * The payment tracker start point.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 14.03.2016
 */
public class TrackerCli {

    private BeanFactory beanFactory = BeanFactory.getInstance();


    public void exec(final String[] args) throws InvalidCommandException {

        Scanner sc = new Scanner(System.in);
        CommandParser commandParser = beanFactory.getCommandParser();
        CommandProcessor commandProcessor = beanFactory.getCommandProcessor();

        commandParser.printHelp();

        //try import payments from file
        if (ArrayUtils.isNotEmpty(args)) {
            Command fileCommand = commandParser.parseCommand(args);
            if (fileCommand == null || !fileCommand.getCommandType().equals(CommandType.IMPORT_FILE)) {
                commandParser.printHelp();

                throw new InvalidCommandException("Failed to import payments");
            }

            commandProcessor.processCommand(fileCommand);
        }


        startPrintScheduller();

        while (true) {

            String line = sc.nextLine();
            try {
                Command command = commandParser.parseCommand(line);
                if (command != null) {
                    commandProcessor.processCommand(command);
                }

            } catch (InvalidCommandException e) {
                System.out.println(e.getMessage() + "\n\rTo see help, type \"help\"");
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Starts a payment sums printer.
     */
    private void startPrintScheduller() {
        ScheduledExecutorService scheduller = Executors.newScheduledThreadPool(1);
        scheduller.scheduleWithFixedDelay(new PaymentsPrinter(), 1, 1, TimeUnit.MINUTES);
    }


    public static void main(String[] args) throws InvalidCommandException {
        new TrackerCli().exec(args);
    }


    /**
     * Program configuration.
     */
    public static class TrackerConfig {

        public static String EXCHANGE_TO = "USD";
    }
}
