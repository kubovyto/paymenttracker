package cz.bsc.paymenttracker.exception;

/**
 * Exception for invalid user input.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class InvalidCommandException extends Exception {

    public InvalidCommandException() {
    }

    public InvalidCommandException(final String message) {
        super(message);
    }

    public InvalidCommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidCommandException(final Throwable cause) {
        super(cause);
    }

}
