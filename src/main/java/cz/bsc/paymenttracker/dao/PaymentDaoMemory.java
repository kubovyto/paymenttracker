package cz.bsc.paymenttracker.dao;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import cz.bsc.paymenttracker.vo.Payment;


/**
 * Payment dao implementation. Payments are stored in memory.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class PaymentDaoMemory implements PaymentDao {

    /**
     * List of stored payments.
     */
    private final List<Payment> payments = Collections.synchronizedList(new LinkedList<Payment>());

    @Override
    public List<Payment> findAll() {
        return Collections.unmodifiableList(payments);
    }

    @Override
    public Payment insertPayment(final Payment payment) {
        payments.add(payment);
        return payment;
    }

    @Override
    public void clearPayments() {
        payments.clear();
    }
}
