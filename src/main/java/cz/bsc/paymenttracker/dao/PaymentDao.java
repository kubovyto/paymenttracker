package cz.bsc.paymenttracker.dao;

import java.util.List;

import cz.bsc.paymenttracker.vo.Payment;


/**
 * API for payment dao.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public interface PaymentDao {

    /**
     * Finds all payments.
     *
     * @return all stored payments
     */
    List<Payment> findAll();


    /**
     * Insert new payment.
     *
     * @param payment payment to insert
     * @return inserted payment
     */
    Payment insertPayment(Payment payment);


    /**
     * Remove all payments.
     */
    void clearPayments();
}
