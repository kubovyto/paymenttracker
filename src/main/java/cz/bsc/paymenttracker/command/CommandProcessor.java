package cz.bsc.paymenttracker.command;

import cz.bsc.paymenttracker.exception.InvalidCommandException;


/**
 * A program command procesor.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public interface CommandProcessor {

    /**
     * Process the program command.
     *
     * @param command command to process
     * @throws InvalidCommandException if no command match the user input
     */
    void processCommand(Command command) throws InvalidCommandException;

}
