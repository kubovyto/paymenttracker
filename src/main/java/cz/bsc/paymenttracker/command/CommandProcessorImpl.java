package cz.bsc.paymenttracker.command;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.testng.Assert;

import cz.bsc.paymenttracker.TrackerCli;
import cz.bsc.paymenttracker.exception.InvalidCommandException;
import cz.bsc.paymenttracker.factory.BeanFactory;
import cz.bsc.paymenttracker.service.PaymentService;


/**
 * Program command processor.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class CommandProcessorImpl implements CommandProcessor {

    private PaymentService paymentService;

    public CommandProcessorImpl(final PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void processCommand(final Command command) throws InvalidCommandException {
        Assert.assertNotNull(command);

        switch (command.getCommandType()) {
            case LIST_PAYMENTS:
                paymentService.printAllPaymentSums();
                break;
            case PRINT_HELP:
                BeanFactory.getInstance().getCommandParser().printHelp();
                break;
            case IMPORT_FILE:
                importFile(command);
                break;
            case INSERT_PAYMENT:
                insertPayment(command);
                break;
            case SET_EXCHANGE_RATE:
                setExchangeRate(command);
                break;
            case TERMINATE:
                System.exit(0);
                break;
            default:
                throw new NotImplementedException("Command type " + command.getCommandType() + " is not implemented.");
        }
    }

    /**
     * Process command to insert a payment.
     *
     * @param command insert payment command
     * @throws InvalidCommandException payment command does not match a payment pattern
     */
    private void insertPayment(final Command command) throws InvalidCommandException {
        Assert.assertNotNull(command);
        Assert.assertEquals(command.getCommandType(), CommandType.INSERT_PAYMENT,
                "Command is not payment type command.");

        if (command.getArgs() == null || command.getArgs().length != 2) {
            throw new InvalidCommandException("Payment command has to match " + CommandParserImpl.PAYMENT_PATTERN);
        }

        String currency = command.getArgs()[0];
        BigDecimal value = new BigDecimal(command.getArgs()[1]);

        paymentService.insertPayment(currency, value);
    }

    /**
     * Process command to import payments from file.
     *
     * @param command command to import file
     * @throws InvalidCommandException if command in file does not match a payment pattern
     */
    private void importFile(final Command command) throws InvalidCommandException {
        Assert.assertNotNull(command);
        Assert.assertNotNull(command.getArgs()[0]);


        final String fileName = command.getArgs()[0];

        boolean fileValid = false;
        List<Command> importedCommands = new LinkedList<Command>();

        BufferedReader bufferedReader = null;

        System.out.println("Importing payments from file " + fileName);

        try {
            FileReader fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);

            CommandParser commandParser = BeanFactory.getInstance().getCommandParser();

            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println("import payment: " + line);

                Command paymentCommand = commandParser.parseCommand(line);

                if (paymentCommand == null) {
                    continue;
                }

                if (paymentCommand.getCommandType() != CommandType.INSERT_PAYMENT) {
                    throw new InvalidCommandException("Only payment command types can be contained in importing file.");
                }

                importedCommands.add(paymentCommand);

                //read next line
                line = bufferedReader.readLine();
            }

            fileValid = true;
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    System.out.println("Can not close file input stream.");
                }
            }
        }

        if (fileValid) {
            for (Command importedCommand : importedCommands) {
                insertPayment(importedCommand);
            }
            System.out.println("\nPayments imported successfully.");
        }
    }

    /**
     * Sets the currency exchange rate to {@link TrackerCli.TrackerConfig#EXCHANGE_TO}.
     *
     * @param command command with exchange rate
     */
    private void setExchangeRate(final Command command) {
        paymentService.setExchangeRate(command.getArgs()[0], new BigDecimal(command.getArgs()[1]));
    }
}
