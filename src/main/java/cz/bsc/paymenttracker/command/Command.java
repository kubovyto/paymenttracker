package cz.bsc.paymenttracker.command;

import java.util.Arrays;

import javax.annotation.Nullable;


/**
 * A program command to be processed.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class Command {

    private CommandType commandType;
    private String[] args;

    public Command(final CommandType commandType) {
        this(commandType, null);
    }

    public Command(final CommandType commandType, @Nullable final String... args) {
        this.commandType = commandType;
        this.args = args;
    }

    public CommandType getCommandType() {
        return commandType;
    }


    public String[] getArgs() {
        return args;
    }

    @Override
    public String toString() {
        return "Command{" +
                "commandType=" + commandType +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}

