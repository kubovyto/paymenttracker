package cz.bsc.paymenttracker.command;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

import cz.bsc.paymenttracker.TrackerCli;
import cz.bsc.paymenttracker.exception.InvalidCommandException;


/**
 * Command parser to parse user input into program command.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class CommandParserImpl implements CommandParser {

    public static final Pattern PAYMENT_PATTERN = Pattern.compile("^([A-Z]{3} -?\\d{1,12}(\\.\\d{1,3})?)$");

    public static final Pattern EXCNAHGE_RATE_PATTERN = Pattern.compile("^([A-Z]{3} \\d{1,12}(\\.\\d{1,10})?)$");


    private Options commandOptions = createOptions();

    private CommandLineParser commandLineParser = new DefaultParser();


    @Nullable
    @Override
    public Command parseCommand(final String line) throws InvalidCommandException {
        if (StringUtils.isEmpty(line)) {
            return null;
        }

        String lineTrim = line.trim();

        Command result;
        if (matchPayment(lineTrim)) {
            String[] tokens = parseTokens(lineTrim);
            result = new Command(CommandType.INSERT_PAYMENT, tokens);
        } else {
            lineTrim = StringUtils.startsWith(lineTrim, "-") ? lineTrim : "-" + lineTrim;
            result = parseOptions(parseTokens(lineTrim), commandOptions);
        }

        if (result == null) {
            throw new InvalidCommandException("Unknown command: " + lineTrim);
        }

        return result;
    }

    @Override
    public Command parseCommand(final String[] args) throws InvalidCommandException {
        return parseOptions(args, commandOptions);
    }

    @Override
    public void printHelp() {
        System.out.println(
                "This program is used for tracking and storing payments. The payment amounts are stored in different currencies.");
        System.out.println(
                "To insert payment, type currency format and the amount in format like CURRENCY AMOUNT, where currency is 3-letter uppercase word and numeric amount.");
        System.out.println("Example:  USD 200\n\r");
        System.out.println("Other options: \n");

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Payment tracker", commandOptions);
    }

    /**
     * Check if the user input matches a payment command.
     *
     * @param lineTrim user input text
     * @return true if text matches the payment command, false otherwise
     */
    private boolean matchPayment(final String lineTrim) {
        Matcher paymentMatcher = PAYMENT_PATTERN.matcher(lineTrim);
        return paymentMatcher.matches();
    }

    /**
     * Tries to parse other user commands.
     *
     * @param args    input arguments
     * @param options possible options
     * @return program command or null, if no command match the user input
     * @throws InvalidCommandException if no command match the user input
     */
    @Nullable
    private Command parseOptions(final String[] args, final Options options)
            throws InvalidCommandException {

        try {
            CommandLine cli = commandLineParser.parse(options, args);

            if (cli.hasOption("file")) {
                return new Command(CommandType.IMPORT_FILE, cli.getOptionValue("file"));
            } else if (cli.hasOption("help")) {
                return new Command(CommandType.PRINT_HELP);
            } else if (cli.hasOption("list")) {
                return new Command(CommandType.LIST_PAYMENTS);
            } else if (cli.hasOption("rate")) {
                String currency = cli.getOptionValue("rate");
                String rate = cli.getArgs() == null || cli.getArgs().length < 1 ? "" : cli.getArgs()[0];

                String exchangeRate = currency + " " + rate;
                Matcher matcher = EXCNAHGE_RATE_PATTERN.matcher(exchangeRate);
                if (matcher.matches()) {
                    return new Command(CommandType.SET_EXCHANGE_RATE, currency, rate);
                } else {
                    throw new InvalidCommandException(
                            "Exchange rate command has to match regular expression " + EXCNAHGE_RATE_PATTERN.pattern());
                }
            } else if (cli.hasOption("quit")) {
                return new Command(CommandType.TERMINATE);
            }

            return null;

        } catch (ParseException e) {
            throw new InvalidCommandException(e);
        }
    }

    /**
     * Parse user input text into tokens (arguments).
     *
     * @param line user input text
     * @return input tokens
     */
    private String[] parseTokens(final String line) {
        return StringUtils.split(line.trim(), " ");
    }


    /**
     * Creates a program input options.
     *
     * @return program input options
     */
    private Options createOptions() {
        Options options = new Options();
        options.addOption("l", "list", false, "Print list of payments.");
        options.addOption("h", "help", false, "Print commands help.");
        options.addOption("f", "file", true, "Import payments from file. eg: file payments.txt");
        options.addOption("r", "rate", true,
                "Set exchange rate to " + TrackerCli.TrackerConfig.EXCHANGE_TO + ". eg: CZE 0.035");
        options.addOption("quit", false, "Terminates the program");
        return options;
    }
}
