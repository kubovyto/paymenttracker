package cz.bsc.paymenttracker.command;

import cz.bsc.paymenttracker.exception.InvalidCommandException;


/**
 * API to parse user input text into program commands.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public interface CommandParser {

    /**
     * Parses user input text into program commands.
     *
     * @param line user input
     * @return program commands
     * @throws InvalidCommandException if no command match the user input
     */
    Command parseCommand(String line) throws InvalidCommandException;


    /**
     * Parses user input arguments into program commands.
     *
     * @param args user input arguments
     * @return program commands
     * @throws InvalidCommandException if no command match the user input
     */
    Command parseCommand(String[] args) throws InvalidCommandException;


    /**
     * Prints program input help.
     */
    void printHelp();
}
