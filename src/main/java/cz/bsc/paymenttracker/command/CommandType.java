package cz.bsc.paymenttracker.command;

/**
 * A program command type.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public enum CommandType {

    LIST_PAYMENTS,

    PRINT_HELP,

    IMPORT_FILE,

    SET_EXCHANGE_RATE,

    INSERT_PAYMENT,

    TERMINATE;

}
