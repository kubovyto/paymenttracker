package cz.bsc.paymenttracker.factory;

import cz.bsc.paymenttracker.command.CommandParser;
import cz.bsc.paymenttracker.command.CommandParserImpl;
import cz.bsc.paymenttracker.command.CommandProcessor;
import cz.bsc.paymenttracker.command.CommandProcessorImpl;
import cz.bsc.paymenttracker.dao.PaymentDao;
import cz.bsc.paymenttracker.dao.PaymentDaoMemory;
import cz.bsc.paymenttracker.service.PaymentService;
import cz.bsc.paymenttracker.service.PaymentServiceImpl;


/**
 * Bean creation factory.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class BeanFactory {

    private static BeanFactory INSTANCE;


    private static PaymentDao paymentDao;
    private static CommandProcessor commandProcessor;
    private static PaymentService paymentService;
    private static CommandParser commandParser;


    private BeanFactory() {

    }

    public PaymentDao getPaymentDao() {
        if (paymentDao == null) {
            paymentDao = new PaymentDaoMemory();
        }
        return paymentDao;
    }

    public CommandProcessor getCommandProcessor() {
        if (commandProcessor == null) {
            commandProcessor = new CommandProcessorImpl(getPaymentService());
        }
        return commandProcessor;
    }

    public PaymentService getPaymentService() {
        if (paymentService == null) {
            paymentService = new PaymentServiceImpl(getPaymentDao());
        }
        return paymentService;
    }

    public CommandParser getCommandParser() {
        if (commandParser == null) {
            commandParser = new CommandParserImpl();
        }

        return commandParser;
    }


    public static BeanFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BeanFactory();
        }
        return INSTANCE;
    }
}
