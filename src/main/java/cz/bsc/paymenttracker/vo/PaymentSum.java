package cz.bsc.paymenttracker.vo;

import java.math.BigDecimal;

import org.testng.Assert;


/**
 * Payment containing sum of all currency payments.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class PaymentSum implements Comparable<PaymentSum> {

    private String currency;
    private BigDecimal value = new BigDecimal(0);

    public PaymentSum(final String currency) {
        Assert.assertNotNull(currency, "Currency can not be null");

        this.currency = currency;
    }

    /**
     * Adds currency value to sum.
     *
     * @param addValue value to add
     */
    public void addValue(final BigDecimal addValue) {
        Assert.assertNotNull(addValue, "Value to add can not be null");
        value = value.add(addValue);
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return currency + " " + value;
    }

    public int compareTo(final PaymentSum o) {
        return currency.compareTo(o.getCurrency());
    }
}
