package cz.bsc.paymenttracker.vo;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

import cz.bsc.paymenttracker.dao.PaymentDao;


/**
 * Payment info domain object stored in {@link PaymentDao}.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class Payment {

    private String currency;
    private BigDecimal value;
    private DateTime transactionTime;

    public Payment(final String currency, final BigDecimal value, final DateTime transactionTime) {
        this.currency = currency;
        this.value = value;
        this.transactionTime = transactionTime;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public DateTime getTransactionTime() {
        return transactionTime;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Payment payment = (Payment) o;

        return new EqualsBuilder()
                .append(currency, payment.currency)
                .append(value, payment.value)
                .append(transactionTime, payment.transactionTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(currency)
                .append(value)
                .append(transactionTime)
                .toHashCode();
    }
}
