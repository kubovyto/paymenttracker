package cz.bsc.paymenttracker.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.joda.time.DateTime;
import org.testng.Assert;

import cz.bsc.paymenttracker.TrackerCli;
import cz.bsc.paymenttracker.dao.PaymentDao;
import cz.bsc.paymenttracker.vo.Payment;
import cz.bsc.paymenttracker.vo.PaymentSum;


/**
 * Implementation of payment service API.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public class PaymentServiceImpl implements PaymentService {

    private PaymentDao paymentDao;

    private Map<String, BigDecimal> exchangeRateMap = new HashMap<String, BigDecimal>();

    public PaymentServiceImpl(final PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }


    @Override
    public void insertPayment(final String currency, final BigDecimal value) {
        paymentDao.insertPayment(new Payment(currency, value, DateTime.now()));
    }


    @Override
    public Collection<PaymentSum> getAllPaymentSums() {

        TreeMap<String, PaymentSum> paymentSumMap = new TreeMap<String, PaymentSum>();

        for (Payment payment : paymentDao.findAll()) {
            PaymentSum currencySum = paymentSumMap.get(payment.getCurrency());
            if (currencySum == null) {
                currencySum = new PaymentSum(payment.getCurrency());
                paymentSumMap.put(payment.getCurrency(), currencySum);
            }
            currencySum.addValue(payment.getValue());
        }

        return Collections.unmodifiableCollection(paymentSumMap.values());
    }


    @Override
    public void setExchangeRate(final String currency, final BigDecimal rate) {
        Assert.assertNotNull(currency);
        Assert.assertNotNull(rate);

        exchangeRateMap.put(currency, rate);
    }

    @Override
    public void printAllPaymentSums() {

        Collection<PaymentSum> allPaymentSums = getAllPaymentSums();
        if (allPaymentSums.isEmpty()) {
            System.out.println("No tracked payments.");
            return;
        }


        System.out.println("\n\rPayment sums:");

        for (PaymentSum paymentSum : allPaymentSums) {
            if (paymentSum.getValue().intValue() != 0) {
                printPaymentSum(paymentSum);
            }
        }
    }

    /**
     * Prints a payment sum. If exchange rate is defined, append exchanged value.
     *
     * @param paymentSum sum to print
     */
    private void printPaymentSum(final PaymentSum paymentSum) {
        String currencySum = paymentSum.toString();

        if (exchangeRateMap.containsKey(paymentSum.getCurrency())) {
            BigDecimal exchangeRate = exchangeRateMap.get(paymentSum.getCurrency());

            BigDecimal result = paymentSum.getValue().multiply(exchangeRate);
            BigDecimal rounded = result.round(new MathContext(3, RoundingMode.CEILING));
            currencySum += " (" + TrackerCli.TrackerConfig.EXCHANGE_TO + " " + rounded + ")";
        }

        System.out.println(currencySum);
    }
}
