package cz.bsc.paymenttracker.service;

import java.math.BigDecimal;
import java.util.Collection;

import cz.bsc.paymenttracker.TrackerCli;
import cz.bsc.paymenttracker.vo.PaymentSum;


/**
 * API to payment service.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 15.03.2016
 */
public interface PaymentService {

    /**
     * Collect all payments into groups by currency.
     *
     * @return payments groups by currency
     */
    Collection<PaymentSum> getAllPaymentSums();


    /**
     * Insert new payment into storage.
     *
     * @param currency payment currency
     * @param value    payment value
     */
    void insertPayment(String currency, BigDecimal value);


    /**
     * Prints all payment groups to standard output.
     */
    void printAllPaymentSums();


    /**
     * Sets the currency exchange rate to {@link TrackerCli.TrackerConfig#EXCHANGE_TO}.
     *
     * @param currency currency
     * @param rate     exchange rate
     */
    void setExchangeRate(String currency, BigDecimal rate);
}
