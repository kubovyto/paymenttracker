package cz.bsc.paymenttracker;

import cz.bsc.paymenttracker.factory.BeanFactory;
import cz.bsc.paymenttracker.service.PaymentService;


/**
 * Task to print all payment collections.
 *
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class PaymentsPrinter implements Runnable {


    @Override
    public void run() {
        PaymentService paymentService = BeanFactory.getInstance().getPaymentService();
        paymentService.printAllPaymentSums();
    }
}
