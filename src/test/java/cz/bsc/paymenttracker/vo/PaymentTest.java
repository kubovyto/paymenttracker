package cz.bsc.paymenttracker.vo;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class PaymentTest {

    @Test
    public void testPaymentSum() {
        PaymentSum paymentSum = new PaymentSum("CZE");
        Assert.assertEquals(paymentSum.getCurrency(), "CZE");
        paymentSum.addValue(new BigDecimal(20));

        Assert.assertEquals(paymentSum.getValue().intValue(), 20);

        paymentSum.addValue(new BigDecimal(20));
        Assert.assertEquals(paymentSum.getValue().intValue(), 40);
    }

}
