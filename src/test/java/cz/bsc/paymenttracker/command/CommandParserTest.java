package cz.bsc.paymenttracker.command;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import cz.bsc.paymenttracker.factory.BeanFactory;


/**
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class CommandParserTest {

    @Test
    public void testParseCommand() throws Exception{

        CommandParser parser = BeanFactory.getInstance().getCommandParser();

        Command command = parser.parseCommand("USD 200");
        assertEquals(command.getCommandType(), CommandType.INSERT_PAYMENT);
        assertEquals(command.getArgs()[0], "USD");
        assertEquals(command.getArgs()[1], "200");

        command = parser.parseCommand(new String[]{"-file", "payments.txt"});
        assertEquals(command.getCommandType(), CommandType.IMPORT_FILE);
        assertEquals(command.getArgs()[0], "payments.txt");
    }

}
