package cz.bsc.paymenttracker.service;

import java.math.BigDecimal;
import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import cz.bsc.paymenttracker.dao.PaymentDao;
import cz.bsc.paymenttracker.factory.BeanFactory;
import cz.bsc.paymenttracker.vo.PaymentSum;


/**
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class PaymentServiceTest {

    @BeforeTest
    public void init() {
        BeanFactory.getInstance().getPaymentDao().clearPayments();
    }

    @Test
    public void insertPaymentTest() {

        PaymentService paymentService = BeanFactory.getInstance().getPaymentService();
        paymentService.insertPayment("USD", new BigDecimal(10));
        paymentService.insertPayment("USD", new BigDecimal(10));
        paymentService.insertPayment("CZE", new BigDecimal(15));

        PaymentDao paymentDao = BeanFactory.getInstance().getPaymentDao();
        Assert.assertEquals(paymentDao.findAll().size(), 3);

        Collection<PaymentSum> allPaymentSums = paymentService.getAllPaymentSums();
        Assert.assertEquals(allPaymentSums.size(), 2);
    }

}
