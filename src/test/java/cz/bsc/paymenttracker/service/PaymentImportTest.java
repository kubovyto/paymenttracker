package cz.bsc.paymenttracker.service;

import java.math.BigDecimal;
import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import cz.bsc.paymenttracker.command.Command;
import cz.bsc.paymenttracker.command.CommandProcessor;
import cz.bsc.paymenttracker.command.CommandType;
import cz.bsc.paymenttracker.factory.BeanFactory;
import cz.bsc.paymenttracker.vo.PaymentSum;


/**
 * @author Tomáš Kubový [<a href="mailto:kubovyto@gmail.com">kubovyto@gmail.com</a>]
 * @since 16.03.2016
 */
public class PaymentImportTest {

    @BeforeTest
    public void init() throws Exception {
        BeanFactory.getInstance().getPaymentDao().clearPayments();

        CommandProcessor commandProcessor = BeanFactory.getInstance().getCommandProcessor();
        String path = getClass().getResource("/payments.txt").getFile();

        Command fileImportCommand = new Command(CommandType.IMPORT_FILE, path);
        commandProcessor.processCommand(fileImportCommand);
    }

    @Test
    public void testImportPayments() {

        PaymentService paymentService = BeanFactory.getInstance().getPaymentService();
        Collection<PaymentSum> allPaymentSums = paymentService.getAllPaymentSums();

        boolean success = true;
        for (PaymentSum allPaymentSum : allPaymentSums) {

            if (allPaymentSum.getCurrency().equals("USD")) {
                Assert.assertEquals(allPaymentSum.getValue(), new BigDecimal("900"));
            } else if (allPaymentSum.getCurrency().equals("HKD")) {
                Assert.assertEquals(allPaymentSum.getValue(), new BigDecimal("300"));
            } else if (allPaymentSum.getCurrency().equals("RMB")) {
                Assert.assertEquals(allPaymentSum.getValue(), new BigDecimal("2000"));
            } else {
                success = false;
            }
        }

        Assert.assertTrue(success);
    }
}
